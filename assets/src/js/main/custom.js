$(document).ready(function () { 

	const scroller = document.querySelector('.scroller');

	const bodyScrollBar = Scrollbar.init(scroller, {
		damping: 0.1,
		delegateTo: document,
		alwaysShowTracks: true
	});

	ScrollTrigger.scrollerProxy(".scroller", {
		scrollTop(value) {
			if (arguments.length) {
				bodyScrollBar.scrollTop = value;
			}
			return bodyScrollBar.scrollTop;
		}
	});

	bodyScrollBar.addListener(ScrollTrigger.update);


	ScrollTrigger.defaults({
		scroller: scroller
	});




	//$(".fancybox").fancybox();

	//$(".select").selectize({});

	$(".scrollTo").click(function (event) {
		event.preventDefault();
		var id = $(this).attr('href'),
			top = $(id).offset().top;
		$('body,html').animate({ scrollTop: top }, 1500);
	});


	function getLazy() {
		$('.lazy').lazy();
	};

	getLazy();

	$('.h_language').click(function () {
		$(this).toggleClass('active');
		$(this).find('.h_language_in').toggleClass('active');
	});



	// $('.header .main_menu .mm_list li a').click(function(){
	//     alert('111111')
	// })



	// $(".html").easeScroll({
	//     frameRate: 60,
	//     animationTime: 3000,
	//     stepSize: 180,
	//     pulseAlgorithm: 1,
	//     pulseScale: 8,
	//     pulseNormalize: 1, 
	//     accelerationDelta: 20,  
	//     accelerationMax: 1,
	//     keyboardSupport: true,
	//     arrowScroll: 50,
	//     touchpadSupport: true,   
	//     fixedBackground: true
	// });


	$('.burger').click(function () {
		$(this).toggleClass('open');
		$('.hd_menu').slideToggle();
	});

	function hideMm() {
		var $width = $(window).width();
		if ($width > 991) {
			$('.hd_menu').hide();
		}
	}

	$(window).resize(function () {
		hideMm();
	});


	if ($('input').is('.phone')) {
		$('input.phone').inputmask("+7 (999) 999-99-99");
	}

	if ($('input').is('.email')) {
		$('input.email').inputmask({
			mask: "*{1,64}[.*{1,64}][.*{1,64}][.*{1,63}]@-{1,63}.-{1,63}[.-{1,63}][.-{1,63}]",
			greedy: !1,
			onBeforePaste: function (pastedValue, opts) {
				pastedValue = pastedValue.toLowerCase();
				return pastedValue.replace("mailto:", "");
			},
			definitions: {
				'*': {
					validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~\-]",
					cardinality: 1,
					casing: "lower"
				},
				"-": {
					validator: "[0-9A-Za-z-]",
					cardinality: 1,
					casing: "lower"
				}
			}
		});
	};

	$('.f1').parsley();


	// $('.selectize-control').click(function() {
	//     var sInt = $(this).find(".selectize-dropdown-content .option").length;

	//     if (sInt >= 5) {
	//         $('.nicescroll-rails').removeClass('hides');
	//         $(this).find(".selectize-dropdown-content").niceScroll({
	//             autohidemode: false,
	//             cursorwidth: 25,
	//             cursorcolor: '#fff',
	//             cursorborder: '1px solid #b8b8b8',
	//             cursorborderradius: 25,
	//             scrollspeed: 20,
	//             cursorminheight: 24,
	//             cursorfixedheight: 24,
	//             railoffset: { left: 3 }
	//         });
	//     } else if (sInt < 5) {
	//         setTimeout(function() {
	//             $('.nicescroll-rails').addClass('hides');
	//         }, 0);
	//         $(this).find(".selectize-dropdown-content").getNiceScroll().hide();
	//     }
	// });   


	$(".fap").fancybox({
		padding: [0, 0, 0, 0],
		fitToView: false,
		tpl: {
			wrap: '<div class="fancybox-wrap fop" tabIndex="-1"><div class="fancybox-skin"><div class="fancybox-outer"><div class="fancybox-inner"></div></div></div></div>',
			closeBtn: '<div title="Close" class="fancybox-item fancybox-close close_fop"></div>'
		},
	});



	function testWebP(callback) {
		var webP = new Image();
		webP.onload = webP.onerror = function () {
			callback(webP.height == 2);
		};
		webP.src = 'data:image/webp;base64,UklGRjoAAABXRUJQVlA4IC4AAACyAgCdASoCAAIALmk0mk0iIiIiIgBoSygABc6WWgAA/veff/0PP8bA//LwYAAA';
	};

	testWebP(function (support) {
		//document.body.innerHTML = support ? 'webP' : 'no_webP'; 
		if (support) {
			$('html').addClass('webP');
		} else {
			$('html').addClass('no_webP');
		}
	});


	const select = (e) => document.querySelector(e);
	function initScrollTo() {
		// find all links and animate to the right position
		gsap.utils.toArray('.linkTo').forEach(link => {
			const target = link.getAttribute('href');
			link.addEventListener('click', (e) => {
				e.preventDefault();
				//console.log(select(target));
				bodyScrollBar.scrollIntoView(select(target), { damping: 0.07, offsetTop: 0 });
			});
		});
	}
	initScrollTo();


	ScrollTrigger.matchMedia({


		// desktop
		"(min-width: 320px) and (max-width: 760px)": function () {
			// setup animations and ScrollTriggers for screens 800px wide or greater (desktop) here...
			// These ScrollTriggers will be reverted/killed when the media query doesn't match anymore.
			//console.log("(min-width: 320px) and (max-width: 760px)")
		},
		// mobile
		"(min-width: 761px) and (max-width: 1000px)": function () {
			// The ScrollTriggers created inside these functions are segregated and get
			// reverted/killed when the media query doesn't match anymore. 
			//console.log("(min-width: 761px) and (max-width: 1000px)")
		},
		"(min-width: 1001px) and (max-width: 1200px)": function () {
			// The ScrollTriggers created inside these functions are segregated and get
			// reverted/killed when the media query doesn't match anymore. 
			//console.log("(min-width: 1001px) and (max-width: 1200px)")
		},
		"(min-width: 1201px) and (max-width: 1366px)": function () {
			// The ScrollTriggers created inside these functions are segregated and get
			// reverted/killed when the media query doesn't match anymore. 
			//console.log("(min-width: 1201px) and (max-width: 1366px)")
		},
		"(min-width: 1367px) and (max-width: 1450px)": function () {
			// The ScrollTriggers created inside these functions are segregated and get
			// reverted/killed when the media query doesn't match anymore. 
			//console.log("(min-width: 1367px) and (max-width: 1450px)")
		},
		"(min-width: 1451px) and (max-width: 6500px)": function () {
			// The ScrollTriggers created inside these functions are segregated and get
			// reverted/killed when the media query doesn't match anymore. 
			//console.log("(min-width: 1451px) and (max-width: 6500px)")
		},
		// all 
		"all": function () {



			//let sin = gsap.utils.toArray('.section_in');

			ScrollTrigger.create({
				trigger: "#header",
				start: "top top",
				end: "top top-=5256",
				pin: true,
				pinSpacing: false,	
				//endTrigger: "qw", 				
			});

			// let $bgLnv = gsap.timeline({
			// 	scrollTrigger: {
			// 		id: 'x',
			// 		trigger: '.lnv_over',
			// 		start: "top top", 
			// 		end: "top top-=5256", 
			// 		pin: true,
			// 		pinSpacing: false,					
			// 		scrub: true, 					           
			// 	},
			// });
			// $bgLnv
			// 	.add('l01')
			// 	.to('.lnv01', 3, {y: '-4%', transformOrigin: '50% 50%', yoyo: true, ease: 'none', repeat: -1}, 'l01')




			// ScrollTriggers created here aren't associated with a particular media query,
			// so they persist.
			// var tlMp01 = gsap.timeline();
			// tlMp01
			//     .add('mpf')
			//     .to(".rows_ft", 1, { y: 0, autoAlpha: 1, ease: "none" }, 'mpf')
			//     .to(".rows_md", 1, { y: 0, autoAlpha: 1, ease: "none" }, 'mpf')
			//     .to(".rows_bt", 1, { y: 0, autoAlpha: 1, ease: "none" }, 'mpf');

			// var $hL = $("#footer").height(),
			//     $hW = $(window).height(),
			//     $d = $hW - $hL;
			// //console.log($d)

			// ScrollTrigger.create({
			//     trigger: "#footer",
			//     id: "footer",
			//     //animation: tlMp01,
			//     pinSpacing: false,
			//     pin: true,
			//     scrub: true,
			//     //markers: true,
			//     // markers: {
			//     //     startColor: "black", 
			//     //     endColor: "black", 
			//     //     fontSize: "18px" 
			//     // }, 
			//     start: "top top+=" + $d, // position of trigger meets the scroller position
			//     //end: "bottom bottom", 
			//     endTrigger: "#ff",
			// });
			var $hL = $("#footer").height(),
				$hW = $(window).height(),
				$d = $hW - $hL;
			//console.log($d)

			ScrollTrigger.create({
				trigger: "#footer",
				//id: "footer",
				//animation: tlMp01,
				pinSpacing: false,
				pin: true,
				scrub: true,
				//markers: true,
				// markers: {
				//     startColor: "black", 
				//     endColor: "black", 
				//     fontSize: "18px" 
				// }, 
				start: "top top+=" + $d, // position of trigger meets the scroller position
				//end: "bottom bottom", 
				endTrigger: "#ff",
			});

		}


	});












});


(function ($) {
	// console.log(navigator.userAgent);
	/* Adjustments for Safari on Mac */
	if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Mac') != -1 && navigator.userAgent.indexOf('Chrome') == -1) {
		// console.log('Safari on Mac detected, applying class...');
		$('html').addClass('safari_mac'); // provide a class for the safari-mac specific css to filter with
	}
})(jQuery); 

// for Mac add class - .desktop .portrait .safari_mac
// for Windows add class -  .desktop .portrait
// for Andriod add class - .android .mobile .portrait .touch
// for iOS add class - .ios .iphone .mobile .portrait .safari_mac .touch




// (function($) {
// 	$(function() {
// 		$('.tabs__caption').on('click', 'li:not(.active)', function() {
// 			$(this)
// 				.addClass('active').siblings().removeClass('active')
// 				.closest('.tabs').find('.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
// 		});
// 	});
// })(jQuery);