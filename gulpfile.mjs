// пути
const PATH_BUILD = './assets/build';
const PATH_SCR = './assets/src';
const PATH_BUILD_HTML = 'assets/build/';
const PATH_BUILD_JS = 'assets/build/js/';
const PATH_BUILD_CSS = 'assets/build/css/';
const PATH_BUILD_IMG = 'assets/build/img/';
const PATH_BUILD_FONTS = 'assets/build/fonts/';
const PATH_BUILD_VIDEO = 'assets/build/video/';

const PATH_SRC_HTML = 'assets/src/*.html';

const PATH_SRC_VIDEO = 'assets/src/video/**/*.*';

const PATH_SRC_JQ = 'assets/src/js/jquery-3.6.0.min.js';
const PATH_SRC_JS_MAIN = 'assets/src/js/main/main.js';
const PATH_SRC_JS_CONTACTS = 'assets/src/js/contacts/contacts.js';

const PATH_SRC_CSS = 'assets/src/styles/main.sass';

const PATH_SRC_CSSLIB = 'assets/src/lib/**/*.css';

const PATH_SRC_IMG = 'assets/src/img/**/*.*';
const PATH_SRC_FONTS = 'assets/src/fonts/**/*.ttf';

const PATH_WATCH_HTML = 'assets/src/**/*.html';
const PATH_WATCH_JS = 'assets/src/js/**/*.js';
const PATH_WATCH_CSS = 'assets/src/styles/**/*.sass';
const PATH_WATCH_IMG = 'assets/src/img/**/*.*';
const PATH_WATCH_FONTS = 'assets/src/fonts/**/*.ttf';


const PATH_CLEAN = './assets/build/*';

// Gulp
import gulp from 'gulp';
// сервер для работы и автоматического обновления страниц
import sync from 'browser-sync';
import rigger from 'gulp-rigger'; // модуль для импорта содержимого одного файла в другой
import compilerSass from 'sass';
import gulpSass from 'gulp-sass'; // модуль для компиляции SASS (SCSS) в CSS
import autoprefixer from 'gulp-autoprefixer'; // модуль для автоматической установки автопрефиксов
import cleanCss from 'gulp-clean-css'; // плагин для минимизации CSS
import uglify from 'gulp-uglify-es'; // модуль для минимизации JavaScript
import cache from 'gulp-cache'; // модуль для кэширования
import del from 'del'; // плагин для удаления файлов и каталогов
import rename from 'gulp-rename';
import imagemin from 'gulp-imagemin'; // плагин для сжатия PNG, JPEG, GIF и SVG изображений
import gifsicle from 'imagemin-gifsicle';
import mozjpeg from 'imagemin-mozjpeg';
import optipng from 'imagemin-optipng';
import svgo from 'imagemin-svgo';
import notify from 'gulp-notify';
import concat from 'gulp-concat';
import webp from 'gulp-webp';
import sourcemaps from 'gulp-sourcemaps';
import gcmq from 'gulp-group-css-media-queries';
import webpCss from 'gulp-webp-css';
import sharpResponsive from "gulp-sharp-responsive";
//import postcss from 'gulp-postcss';
import buffer from "vinyl-buffer";

import changed from 'gulp-changed';
import ttf2woff2 from'gulp-ttf2woff2';
import cached from 'gulp-cached';

const browserSync = sync.create();
const sass = gulpSass(compilerSass);


// запуск сервера
gulp.task('browser-sync', () => {
  browserSync.init({
    server: {
      baseDir: PATH_BUILD
    },
    notify: false
  })
});

// сбор html
gulp.task('html:build', () => {
  return gulp.src(PATH_SRC_HTML) // выбор всех html файлов по указанному пути
    .pipe(rigger()) // импорт вложений
    .pipe(buffer())
    .pipe(gulp.dest(PATH_BUILD_HTML)) // выкладывание готовых файлов
    .pipe(browserSync.reload({ stream: true })) // перезагрузка сервера
});

// сбор lib стилей
gulp.task('cssLib:build', () => {
  return gulp.src(PATH_SRC_CSSLIB) // получим все файлы css
    //.pipe(sass({outputStyle: 'expanded'}).on('error', notify.onError())) // scss -> css
    //.pipe(buffer())
    .pipe(concat('lib.css')) //Объединение файлов в один
    .pipe(autoprefixer()) // добавим префиксы
    .pipe(gulp.dest(PATH_BUILD_CSS))
    .pipe(rename({suffix: '.min'}))
    .pipe(cleanCss()) // минимизируем CSS
    .pipe(gulp.dest(PATH_BUILD_CSS))
    .pipe(browserSync.stream()) // перезагрузим сервер 
});

// сбор стилей
gulp.task('css:build', () => {
  return gulp.src(PATH_SRC_CSS) // получим main.scss
    //.pipe(sourcemaps.init({largeFile: true, loadMaps: true}))
    .pipe(sass({outputStyle: 'expanded'}).on('error', notify.onError())) // scss -> css
    .pipe(gcmq())
    .pipe(autoprefixer()) // добавим префиксы
    .pipe(gulp.dest(PATH_BUILD_CSS))
    .pipe(webpCss())
    .pipe(rename({suffix: '.min'}))
    .pipe(cleanCss()) // минимизируем CSS
    //.pipe(sourcemaps.write())
    .pipe(gulp.dest(PATH_BUILD_CSS))
    .pipe(browserSync.stream()) // перезагрузим сервер
});

// сбор js
gulp.task('js:build', () => { 
  return gulp.src(PATH_SRC_JS_MAIN) // получим файл main.js
    .pipe(buffer())
    .pipe(rigger()) // импортируем все указанные файлы в main.js
    .pipe(cached('linting'))
    .pipe(gulp.dest(PATH_BUILD_JS))
    .pipe(rename({ suffix: '.min' }))
    //.pipe(sourcemaps.init({largeFile: true}))
    .pipe(uglify.default()) // минимизируем js
    //.pipe(sourcemaps.write())
    .pipe(gulp.dest(PATH_BUILD_JS)) // положим готовый файл
    .pipe(browserSync.reload({ stream: true })) // перезагрузим сервер
});

gulp.task('jsContacts:build', () => { 
  return gulp.src(PATH_SRC_JS_CONTACTS) // получим файл main.js
    .pipe(buffer())
    .pipe(rigger()) // импортируем все указанные файлы в main.js
    .pipe(cached('linting'))
    .pipe(gulp.dest(PATH_BUILD_JS))
    .pipe(rename({ suffix: '.min' }))
    //.pipe(sourcemaps.init({largeFile: true}))
    .pipe(uglify.default()) // минимизируем js
    //.pipe(sourcemaps.write())
    .pipe(gulp.dest(PATH_BUILD_JS)) // положим готовый файл
    .pipe(browserSync.reload({ stream: true })) // перезагрузим сервер
});

// перенос jQuery
gulp.task('jQ:build', () => { 
  return gulp.src(PATH_SRC_JQ) // получим файл main.js
    .pipe(buffer())
    .pipe(cached('linting'))
    .pipe(gulp.dest(PATH_BUILD_JS))
    .pipe(gulp.dest(PATH_BUILD_JS)) // положим готовый файл
    .pipe(browserSync.reload({ stream: true })) // перезагрузим сервер
});

// перенос Video
gulp.task('video:build', () => { 
  return gulp.src(PATH_SRC_VIDEO) // получим файл main.js
    .pipe(buffer())
    .pipe(gulp.dest(PATH_BUILD_VIDEO)) // положим готовый файл
    .pipe(browserSync.reload({ stream: true })) // перезагрузим сервер
});

// перенос шрифтов
gulp.task('fonts:build', () => {
  return gulp.src(PATH_SRC_FONTS)
    .pipe(changed(PATH_BUILD_FONTS, {
      extension: '.woff2',
      hasChanged: changed.compareLastModifiedTime
    }))
    .pipe(ttf2woff2())
    .pipe(buffer())
    .pipe(gulp.dest(PATH_BUILD_FONTS))
});
 
// обработка картинок
gulp.task('image:build', () => {
  return gulp.src(PATH_SRC_IMG)
    .pipe(gulp.dest(PATH_BUILD_IMG))
    .pipe(buffer()) 
    .pipe(cache(imagemin([
      gifsicle({ interlaced: true }),
      mozjpeg({ quality: 85, progressive: true }),
      optipng({ optimizationLevel: 5 }),
      svgo()
    ])))
    .pipe(gulp.dest(PATH_BUILD_IMG))
});

gulp.task('imageCrop:build', () => {
  return gulp.src("assets/src/img/**/*.{jpg,png}")
    //.pipe(gulp.dest(PATH_BUILD_IMG))
    .pipe(buffer())
    .pipe(sharpResponsive({
      includeOriginalFile: true,
      formats: [
        { width: 770, rename: { suffix: "-sm" } },
        { width: 1450, rename: { suffix: "-lg" } },
      ]
    })) 
    .pipe(gulp.dest(PATH_BUILD_IMG))
    .pipe(webp())
    .pipe(gulp.dest(PATH_BUILD_IMG))
});

// удаление каталога build
gulp.task('clean:build', () => {
  //return del(PATH_CLEAN);
  return del(['./assets/build/*', '!assets/build/img', '!assets/build/fonts', '!assets/build/video'])
});

// очистка кэша
gulp.task('cache', () => {
  cache.clearAll();
});

// сборка
gulp.task('build',
  gulp.series('clean:build',
    gulp.parallel(
      'html:build',
      'cssLib:build',
      'css:build',
      'jQ:build',
      'js:build',
      //'jsContacts:build',
      'fonts:build',
      'image:build',
      'imageCrop:build',
      'video:build'
    )
  )
);

// запуск задач при изменении файлов
gulp.task('watch', () => {
  gulp.watch(PATH_WATCH_HTML, gulp.series('html:build'));
  gulp.watch(PATH_WATCH_CSS, gulp.series('cssLib:build'));
  gulp.watch(PATH_WATCH_CSS, gulp.series('css:build'));
  //gulp.watch(PATH_WATCH_JS, gulp.series('jQ:build'));
  gulp.watch(PATH_WATCH_JS, gulp.series('js:build'));
  //gulp.watch(PATH_WATCH_JS, gulp.series('jsContacts:build'));
  gulp.watch(PATH_WATCH_IMG, gulp.series('image:build'));
  gulp.watch(PATH_WATCH_IMG, gulp.series('imageCrop:build'));
  gulp.watch(PATH_WATCH_FONTS, gulp.series('fonts:build'));
});

// задача по умолчанию
gulp.task('start', gulp.series(
  'build',
  gulp.parallel('browser-sync','watch', 'cache')
));
